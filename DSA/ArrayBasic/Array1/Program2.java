//Count ni. of elements having atleast 1 element greater than itself 
//arr : [2,5,1,4,8,0,8,1,3,8];
//op : 7

class YASH{
	public static void main(String [] args){
		int count = 0 ;
		int N = 10;
		int arr[] = new int[]{1,2,3,4,5,6,7,8,9,0};
		//Max Element
		int maxEle = Integer.MIN_VALUE;
		for(int i = 0 ; i < N ; i++ ){
			if(arr[i] > maxEle){
				maxEle = arr[i];
			}
		}

		//Count 
		for(int i=0; i<N; i++){
			if(arr[i] ==  maxEle){
				count++;
			}
		}

		System.out.println(maxEle-count);
	}
}

