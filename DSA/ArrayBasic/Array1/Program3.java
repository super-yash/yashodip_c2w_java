//return the count of pairs(i,j) with Arr[i] + Arr[j] = k 
//Arr: [1,2,3,4,5,6,7,8,9,2,3,4,6,7,7]
//N : 10 
//K : 10  NOTE : i != j


class YASH{
	public static void main(String[] args){
		int arr[] = new int[]{1,2,3,4,5,6,7,8,9,2,3,4,6,7,7};
		int k = 10;
		int n = 10;
		int count = 0;
		for(int i=0;  i<n ;i++){
			for(int j=i+1 ; j<n ; j++){
				if(arr[j]+arr[i] == k && i != j){
					count++;
				}
			}
		}
		System.out.println("Count of pairs is : " + count);
	}
}
