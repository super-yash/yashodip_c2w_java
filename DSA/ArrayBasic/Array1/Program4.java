// Reverse array without using extra space 
// Time Complexity : O(N)
// Arr : [8,4,2,7,1,5,8,3]
// OP : Arr : [3,8,5,1,7,2,4,8]

class YASH{
	public static void main(String[] args){
		int arr[] = new int[]{8,4,2,7,1,5,8,3};
		int i = 0;
		int j = arr.length-1;

		while(i<j){
			arr[i] = arr[i] + arr[j];
			arr[j] = arr[i] - arr[j];
			arr[i] = arr[i] - arr[j];

			i++;
			j--;
		}
		for(int x:arr){
			System.out.print(x + "\t");
		}
		System.out.println();
	}
}
