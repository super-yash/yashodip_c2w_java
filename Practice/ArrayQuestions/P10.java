// Sort array in ascending order

import java.util.*;

class yash {

        public static void main(String[] args){
	
                Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size : ");
                int size = sc.nextInt();

                int arr[] =  new int[size];
		
		System.out.println("Enter Elements ");
                for( int i = 0 ;  i < size ; i++ ){
                        arr[i] = sc.nextInt();
                }

		System.out.println("Output :");
                for( int i = 0 ; i < size ; i++ ){
			for( int j = i+1 ; j < size ; j++ ){
				int temp = 0 ;
				if( arr[i] > arr[j]){
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp ;
				}
			}
			System.out.println(arr[i]);
		}

	}
}
