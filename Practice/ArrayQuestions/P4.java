//Taking N numbers of values in the array and Print only Even Numbers 
//

import java.util.*;

class yash {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the Size : ");
		int size = sc.nextInt();

		int []arr = new int[size];

		for( int i = 0 ; i < size ; i++ ){
			
			arr[i] = sc.nextInt();
		}

		System.out.println();
		for(int i = 0  ; i < size ; i++ ){
		
			if( arr[i] % 2 == 0 ){
			
				System.out.print(arr[i] + "\t" );
			}
		}	
		System.out.println();

	}

}
