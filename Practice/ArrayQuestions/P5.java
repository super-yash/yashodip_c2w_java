// Find the maximum number present in array

import java.util.*;

class yash {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size : ");
		int size  = sc.nextInt();

		int arr[] = new int[size];

		for( int i = 0 ; i < size ; i++ ){
		
			arr[i] = sc.nextInt();
		}

		int max = arr[0];
		for( int i = 1 ; i < size ; i++ ){
			if( max < arr[i] ){
				max = arr[i] ;
			}

		}
		System.out.println( "\nMaximum number is : " +  max);

	}

}



