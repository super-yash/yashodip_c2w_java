 // min number in the array
import java.util.*;

class  yash {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.print("Insert Size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		
		System.out.println("\nEnter the Numbers : ");
		for( int i = 0 ; i < size ; i++ ){
		
			arr[i] = sc.nextInt();
		}

		int min = arr[0];
		for( int i = 1 ; i < size ; i++ ){
		
			if(min > arr[i]){
				min = arr[i];
			}

		}
		System.out.println("Minimum numberr in the array is : " + min );
		
	}

}

