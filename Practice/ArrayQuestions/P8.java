//Frequency of the digits (a number of times appear in array)

import java.util.*;

class yash {

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);

		int size = sc.nextInt();

		int arr[] =  new int[size];

		for( int i = 0 ;  i < size ; i++ ){
		
			arr[i] = sc.nextInt();
		}
		
		for( int i = 0 ; i < size ; i++ ){
		
			int count = 0 ;
			for( int j = 0 ; j < size ; j++ ){
			
				if( arr[i] == arr[j] ){
					
					count++;
				}
			}
			System.out.println("Frequency of " + arr[i] + " is " + count );
		}
	}
}
