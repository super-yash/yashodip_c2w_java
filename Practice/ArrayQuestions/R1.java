/* WAP to take a character array as input, but only print characters do not print special characters
*/
import java.util.*;

class yash {

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Size :");
                int size = sc.nextInt();

                char arr1[] =  new char[size];
                char arr2[] =  new char[size];
		
		System.out.println("Enter Elements in Array1 & Array2 : ");
                for( int i = 0 ;  i < size ; i++ ){

                        arr1[i] = sc.next().charAt(0);
                        arr2[i] = sc.next().charAt(0);
                }
		
		System.out.println();
		for( int i = 0 ; i < size ; i++ ){
		
			if( arr1[i] >= 'a' && arr1[i] <= 'z' || arr1[i] >= 'A' && arr1[i] <= 'Z' ){
				System.out.println(arr1[i]);
		}

		}

	}

}

