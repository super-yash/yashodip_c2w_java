// common elements in array
//


import java.util.*;

class yash {

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size : ");
		int size = sc.nextInt();

		int arr1[] = new int[size];
		int arr2[] = new int[size];

		System.out.println("Array1 Elements ;");
		for( int i = 0 ; i < size ; i++ ){
			arr1[i] = sc.nextInt();
		} 
		
		System.out.println("Array2 Elements ;");
		for( int i = 0 ; i < size ; i++ ){
			arr2[i] = sc.nextInt();
		} 

		System.out.println("Output");
		for(int i = 0 ; i < size ; i++ ){
			for(int j = 0 ; j < size ; j++ ){
				if( arr1[i] == arr2[j]){
					System.out.print(arr1[i] + "\t");
				}
			}
		}
		System.out.println();
	}
}
