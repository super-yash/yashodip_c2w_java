
// Even And Odd Numbers in Array


import java.util.*;

class yash {

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter Size : ");
                int size = sc.nextInt();

                int arr1[] = new int[size];

                System.out.println("Array1 Elements ;");
                for( int i = 0 ; i < size ; i++ ){
                        arr1[i] = sc.nextInt();
                }

		int evn = 0;
		int odd = 0;
		for( int i = 0 ; i < size ; i++ ){
			if( arr1[i] % 2 == 0 ){
				evn++;
			}else{
				odd++;
			}
		}
		System.out.println("Number of Even Elements are : " + evn );
		System.out.println("Number of Odd Elements are : " + odd);

	}

}


