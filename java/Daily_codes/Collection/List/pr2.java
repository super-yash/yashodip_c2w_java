



import java.util.*;

class YASH{

	public static void main(String[] args){
	
		List al = new ArrayList();

		al.add(10);
		al.add(20);
		al.add("Shashi");
		al.add("Kanha");
		al.add(30.5);

		System.out.println(al);

		al.add(3,"Rahul");
		al.add(5,"Aashish");

		System.out.println(al.contains(30));
		System.out.println(al.contains("Shashi"));

		System.out.println(al.get(5));
		System.out.println(al.isEmpty());

		System.out.println(al);

		System.out.println(al.remove(5));
		System.out.println(al.remove("Shashi"));

		System.out.println(al);

		al.set(3,"Badhe");
		System.out.println(al);
		System.out.println(al.size());

		al.clear();
		System.out.println(al);
	}
}
