



import java.util.*;

class YASH{

	public static void main(String[] args){
		
		HashMap hm = new HashMap();

		//put(k,v)
		hm.put("a","Kanha");
		hm.put("c","YASH");
		hm.put("d","RAHUL");
		hm.put("b","ashish");
		hm.put("e","Kanha");

		System.out.println(hm);

		//remove(java.lang.Object)
		hm.remove("c");
		System.out.println(hm + "\n");

		//get(java.lang.Object)
		System.out.println(hm.get("e") + "\n");

		//containsKey(java.lang.Object)
		System.out.println(hm.containsKey("c") + "\n" );

		//containsValue()
		System.out.println(hm.containsValue("Kanha")+ "\n");

		//equals
		System.out.println(hm.equals("e") + "\n");

		//putIfAbsent(k,v)	-Check key is present or not , if not present then add new key value pair 
		hm.putIfAbsent( "m" , "BMW" );
		System.out.println(hm + "\n");

		//replace(k,v) 	 -replacing key value pair
		hm.replace( "d" , "YASHODIP" );
		System.out.println(hm + "\n");
		
		//size()	-Print the size of size of key value pairs available
		System.out.println(hm.size());

		//isEmpty	- checking the HashMap is empty or not
		System.out.println( "\n" + hm.isEmpty());
	}
}
