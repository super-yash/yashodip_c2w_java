
import java.util.*;

class Demo {
    int data;

    Demo(int data) {
        this.data = data;
    }

    public void finalize() {
        System.out.println("In Finalize: " + data);
    }

    public String toString() {
        return data + "";
    }
}

class WeakHashMapDemo {
    public static void main(String[] args) {
        WeakHashMap whm = new WeakHashMap();

        Demo obj1 = new Demo(10);
        Demo obj2 = new Demo(20);
        Demo obj3 = new Demo(30);

        whm.put(obj1, "YASH");
        whm.put(obj2, "GANU");
        whm.put(obj3, "YRT");

        obj1 = null;
        obj2 = null;

        // Explicitly request garbage collection
        System.gc();

        // Print the contents of the WeakHashMap
        System.out.println(whm);
    }
}

