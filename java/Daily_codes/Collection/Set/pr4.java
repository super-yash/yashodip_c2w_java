// COMPARABLE INTERFACE  (java.lang.Comparable;)


import java.util.*;

class YASH implements Comparable<YASH>{

	int num ;
	String Names ;

	YASH(int num , String Names){
		
		this.num = num ;
		this.Names = Names ;
	}

	public int compareTo(YASH obj){
	
		return num - obj.num;
	}

	public String toString(){
	
		return Names ;
	}
}
class Ganu{

	public static void main(String[] args){
	
		SortedSet s = new TreeSet();
		s.add(new YASH(10,"YASH 1"));
		s.add(new YASH(20,"YASH 2"));
		s.add(new YASH(30,"YASH 3"));
		s.add(new YASH(40,"YASH 4"));
		s.add(new YASH(50,"YASH 5"));

		System.out.println(s);

	}
}
