
// Navigable 

import java.util.*;

class YASH {

	public static void main(String[] args){
	
		NavigableSet<Integer> s = new TreeSet<Integer>();

		s.add(10);
		s.add(40);
		s.add(50);
		s.add(20);
		s.add(30);
		s.add(10);

		System.out.println(s);

		//subSet(E,E)
		System.out.println(s.subSet(20,50));

		//headSet(E)   (< less)
		System.out.println(s.headSet(40));

		//tailSet(E)   (<= less than or equal to)
		System.out.println(s.tailSet(40));

		System.out.println(s.first());
		System.out.println(s.last());

		System.out.println("--------------------------------------------------------------------------");
		//lower
		System.out.println(s.lower(40));

		//floor
		System.out.println(s.floor(40));

		//ceiling
		System.out.println(40);

		//higher
		System.out.println(40);
		s.pollFirst();
		s.pollLast();
		System.out.println(s);
	}
}
/*
 *  public java.util.SortedSet<E> subSet(E, E);
  public java.util.SortedSet<E> headSet(E);
  public java.util.SortedSet<E> tailSet(E);
  public java.util.Comparator<? super E> comparator();
  public E first();
  public E last();
  public E lower(E);
  public E floor(E);
  public E ceiling(E);
  public E higher(E);
  public E pollFirst();
  public E pollLast();
  */
