//Average of an Array
//Array Elements are : 
//2
//4
//6
//8

import java.io.*;

class Continental {

        public static void main(String[] args)throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Size : ");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

		int sum = 0 ;
                for( int i = 0 ; i < arr.length ; i++ ){
                        arr[i] = Integer.parseInt(br.readLine());
                }

                for( int i = 0 ; i < arr.length ; i++ ){
                
			sum += arr[i];
		}
		int avg = sum/arr.length;
        	System.out.println( "Array Elements average is  :"  + avg);

        }

}

