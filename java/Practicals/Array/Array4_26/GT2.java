//Difference between MAX and MIN 
//Inputs are 
//3
//6
//9
//8
//10
import java.io.*;

class Continental {

        public static void main(String[] args)throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Size : ");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                for( int i = 0 ; i < arr.length ; i++ ){
                        arr[i] = Integer.parseInt(br.readLine());
                }

		int min = arr[0] ; 
		int max = arr[0] ;
                for( int i = 1 ; i < arr.length ; i++ ){
                
			if( arr[i] < min ){
				min = arr[i];
			}
			if( arr[i] > max ){
				max =arr[i];
			}
		}
		int diff = max-min;
		System.out.println("The difference between maximum and minimum is :" + diff);

        }
}

