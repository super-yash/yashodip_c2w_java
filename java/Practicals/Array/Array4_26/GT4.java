//check user given number occured more than 2 times or equals to 2
//Size : 6 
//56
//65
//78
//56
//90
//56

import java.io.*;

class Continental {

        public static void main(String[] args)throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Size : ");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                for( int i = 0 ; i < arr.length ; i++ ){
                        arr[i] = Integer.parseInt(br.readLine());
                }

		int count = 0 ;	
		System.out.print("Enter the number to check : ");
		int num = Integer.parseInt(br.readLine());
                for( int i = 0 ; i < arr.length ; i++ ){
                
			if( num == arr[i] ){
				count++;
			}
		}
		if( count >= 2 ){
               	 	System.out.println( num + " occurs more than 2 times in the array ");
			}
        }

}

