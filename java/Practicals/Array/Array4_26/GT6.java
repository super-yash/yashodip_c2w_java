import java.io.*;
import java.util.*;

class Continental {

        public static void main(String[] args)throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Size : ");
                int size = Integer.parseInt(br.readLine());

                char arr[] = new char[size];

                for( int i = 0 ; i < size ; i++ ){
                        arr[i] = (char)br.read();
                }

		int countVow = 0 ;
		int countCon = 0 ;
                for( int i = 0 ; i < size ; i++ ){
                
			if( arr[i] == 'a' || arr[i] == 'e' ||  arr[i] == 'i' || arr[i] == 'o' ||arr[i] == 'u' || arr[i] == 'A' ||arr[i] == 'I' || arr[i] == 'E' ||arr[i] == 'O' || arr[i] == 'U' ){
				countVow++;
			}else {
				countCon++;
			}
			
		}
                System.out.println("Count of vowels is : " + countVow);
                System.out.println("Count of Consonants is : " + countCon);

        }

}

