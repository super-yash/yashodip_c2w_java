import java.io.*;

class Continental {

        public static void main(String[] args)throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Size : ");
                int size = Integer.parseInt(br.readLine());

                char arr[] = new char[size];

                for( int i = 0 ; i < arr.length ; i++ ){
                       // arr[i] = br.readLine().charAt(0);
		       arr[i] = (char)br.read();
		       br.skip(1);
                }

                int count = 0 ;
                System.out.print("Enter the character to check : ");
                char ch = br.readLine().charAt(0);
                for( int i = 0 ; i < arr.length ; i++ ){

                        if( ch == arr[i] ){
                                count++;
                        }
                }
                
               System.out.println( ch + " occurs " +  count + " times in the array ");
               
        }

}
