//replace elments with # , which are not in range of a - z 
import java.io.*;

class Continental {

        public static void main(String[] args)throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Size : ");
                int size = Integer.parseInt(br.readLine());

                char arr[] = new char[size];

                for( int i = 0 ; i < arr.length ; i++ ){
                        arr[i] = (char)br.read();
                        br.skip(1);
                }

                for( int i = 0 ; i < arr.length ; i++ ){

                        if(arr[i] <= 'z' && arr[i] >= 'a'){
                                System.out.print(arr[i] + "\t");
                        } else {
                                System.out.print( "#" + "\t");
                        }
                }
                System.out.println();

        }

}
