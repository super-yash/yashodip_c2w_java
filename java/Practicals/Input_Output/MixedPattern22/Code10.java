

import java.io.*;

class JP10 {

        public static void main(String[] YASHODIP) throws IOException {

                BufferedReader br  = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Rows : ");
                int rows = Integer.parseInt(br.readLine());


                for( int i = 1 ; i <= rows ; i++ ){

                        for( int j = 1 ; j < rows-i+1 ; j++ ){
				System.out.print("\t");
                        }
			
			int temp = i ;
                        for( int j = 1 ; j < i*2 ; j++ ){
			
				if( i > j ){
					System.out.print( temp-- + "\t" );
				}else{
					System.out.print( temp + "\t" );
					temp++;
				}
			}

                        System.out.println();
                }

        }

}
