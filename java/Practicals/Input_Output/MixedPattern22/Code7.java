

import java.io.*;

class JP7 {

        public static void main(String[] YASHODIP) throws IOException {

                BufferedReader br  = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter Rows : ");
                int rows = Integer.parseInt(br.readLine());


		int temp = 1 ;
                for( int i = 1 ; i <= rows ; i++ ){
			
			int num = temp ;
                        for( int j = 1 ; j < rows-i+1 ; j++ ){
				System.out.print("\t");
                        }

                        for( int j = 1 ; j < i*2 ; j++ ){
				System.out.print( num-- + "\t");
                        }
			temp += 2 ;

                        System.out.println();
                }

        }

}
