

import java.io.*;

class Varient8 {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Number : ");
		int num = Integer.parseInt(br.readLine());

		int count = 0 ;
		int temp = num ;
		while( temp != 0 ){
			int rem = temp % 10 ;
			if( rem == 0 ){
				count++ ;
			}
			temp /= 10 ;
		}
		if( count > 0 ){
			System.out.println("It is a Duck Number ");
		} else {
			System.out.println("It is not a Duck Number ");
		}

	}

}



