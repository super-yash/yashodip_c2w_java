

import java.io.*;

class Varient9{

	public static void main(String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("\n\nEnter Number : ");
		int num = Integer.parseInt(br.readLine());

		int sum = 0 ;
		int temp = num ;
		while( temp > 0 ){
		
			int rem = temp % 10 ;
			sum += rem ;
			temp /= 10 ;
		}
		if( num % sum == 0 ){
			System.out.println("This is an HARSHAD NUMBER \n\n");
		} else {
			System.out.println("This is not a HARSHAD NUMBER \n\n");
		}

	}
}
