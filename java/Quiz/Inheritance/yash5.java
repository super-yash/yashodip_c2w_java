abstract class Comp_lang{
	abstract String coding();
	abstract int coding_practice();
}
class C_lang extends Comp_lang{
	public String coding(){
		System.out.println("Coding with pointers");
		return "Happy_Coding";
	}
	public int coding_practice(){
		System.out.println("2hrs a Day");
		return 1;
	}
}
class Cpp_lang extends C_lang {
	Cpp_lang(){
		System.out.println("In cpp lang Constructor");
	}
}
class Java_lang extends Cpp_lang{
	
}
class Learner{
	public static void main(String[] args){
		Comp_lang obj = new Java_lang();
		System.out.println(obj.coding());
	}
}
