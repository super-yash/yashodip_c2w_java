import java.util.Scanner;

class OneInMatrix {
   	public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            int arr[][] = new int[5][5];
            
            // Position of 1 in Row , where it will be getiing place
            int posRow = sc.nextInt();
            //Position of 1 in Column , where it will be getting place
            int posCol = sc.nextInt();

            if( posRow < 0 || posCol < 0 || posRow > 4 || posCol > 4){
                System.out.println("Invalid Operation");
            }else{
                arr[posRow][posCol] = 1 ;
                for(int i = 0; i < 5; i++){
                    for(int j = 0; j < 5; j++){
                        System.out.print(arr[i][j] + " ");
                    }
                    System.out.println();
                }
            }
        sc.close();
    }
}
